export enum CharType {
  Others = "0",
  KanjiNum = "M",
  Kanji = "H",
  Hiragana = "I",
  Katakana = "K",
  Alphabet = "A",
  Numbers = "N",
  Internal = "B",
}

const enum Chars {
  M1 = 19968, // 一
  M2 = 20108, // 二
  M3 = 19977, // 三
  M4 = 22235, // 四
  M5 = 20116, // 五
  M6 = 20845, // 六
  M7 = 19971, // 七
  M8 = 20843, // 八
  M9 = 20061, // 九
  M10 = 21313, // 十
  M11 = 30334, // 百
  M12 = 21315, // 千
  M13 = 19975, // 万
  M14 = 20740, // 億
  M15 = 20806, // 兆

  H1a = 19968, // 一
  H1Z = 40864, // 龠
  H2 = 12293, // 々
  H3 = 12294, // 〆
  H4 = 12533, // ヵ
  H5 = 12534, // ヶ
  I1a = 12353, // ぁ
  I1Z = 12435, // ん
  K1a = 12449, // ァ
  K1Z = 12532, // ヴ
  K2 = 12540, // ー
  K3a = 65393, // ｱ
  K3Z = 65437, // ﾝ
  K4 = 65438, // ﾞ
  K5 = 65392, // ｰ

  A1a = 97, // a
  A1Z = 122, // z
  A2a = 65, // A
  A2Z = 90, // Z
  A3a = 65345, // ａ
  A3Z = 65370, // ｚ
  A4a = 65313, // Ａ
  A4Z = 65338, // Ｚ

  N1a = 48, // 0
  N1Z = 57, // 9
  N2a = 65296, // ０
  N2Z = 65305, // ９
}

export function charType(src: string): CharType {
  let charCode = src.charCodeAt(0)

  switch (charCode) {
    case Chars.M1:
    case Chars.M2:
    case Chars.M3:
    case Chars.M4:
    case Chars.M5:
    case Chars.M6:
    case Chars.M7:
    case Chars.M8:
    case Chars.M9:
    case Chars.M10:
    case Chars.M11:
    case Chars.M12:
    case Chars.M13:
    case Chars.M14:
    case Chars.M15:
      return CharType.KanjiNum
      break

    case Chars.H2:
    case Chars.H3:
    case Chars.H4:
    case Chars.H5:
      return CharType.Kanji
      break

    case Chars.K2:
    case Chars.K4:
    case Chars.K5:
      return CharType.Katakana
      break
  }

  // 0-9
  if (Chars.N1a <= charCode && charCode <= Chars.N1Z) {
    return CharType.Numbers
  }

  // A-Z
  if (Chars.A2a <= charCode && charCode <= Chars.A2Z) {
    return CharType.Alphabet
  }

  // a-z
  if (Chars.A1a <= charCode && charCode <= Chars.A1Z) {
    return CharType.Alphabet
  }

  // ぁ-ん
  if (Chars.I1a <= charCode && charCode <= Chars.I1Z) {
    return CharType.Hiragana
  }

  if (Chars.H1a <= charCode && charCode <= Chars.H1Z) {
    return CharType.Kanji
  }

  // ァ-ヴ
  if (Chars.K1a <= charCode && charCode <= Chars.K1Z) {
    return CharType.Katakana
  }

  // ０-９
  if (Chars.N2a <= charCode && charCode <= Chars.N2Z) {
    return CharType.Numbers
  }

  // Ａ-Ｚ
  if (Chars.A4a <= charCode && charCode <= Chars.A4Z) {
    return CharType.Alphabet
  }

  // ａ-ｚ
  if (Chars.A3a <= charCode && charCode <= Chars.A3Z) {
    return CharType.Alphabet
  }

  // ｱ-ﾝ
  if (Chars.K3a <= charCode && charCode <= Chars.K3Z) {
    return CharType.Katakana
  }

  return CharType.Others
}

export type Dictionary = { [key: string]: { [key: string]: number } }
export type Bias = number

export function createJapaneseTokenizer(
  start: Bias,
  dict: Dictionary
): (src: string) => Array<string> {
  var bias = (section: string, key: string): number => {
    if (
      typeof dict[section] !== "undefined" &&
      typeof dict[section][key] !== "undefined"
    ) {
      return dict[section][key]
    }

    return 0
  }

  var tokenizer = (src: string): Array<string> => {
    var result = new Array<string>()

    if (src.length === 0) {
      return result
    }

    var p1 = "U",
      p2 = "U",
      p3 = "U",
      w1 = "B3",
      w2 = "B2",
      w3 = "B1",
      w4 = "E1",
      w5 = "E2",
      c1 = "0",
      c2 = "0",
      c3 = "0",
      c4 = "0",
      c5 = "0",
      c6 = "0"

    var word = ""

    for (let idx = 0, len = src.length; idx < len; idx++) {
      var s1 = "",
        s2 = "",
        s3 = "",
        s4 = "",
        s5 = "",
        s6 = "",
        t1 = CharType.Others,
        t2 = CharType.Others,
        t3 = CharType.Others,
        t4 = CharType.Others,
        t5 = CharType.Others,
        t6 = CharType.Others

      if (idx === 0) {
        s1 = w1
        s2 = w2
        s3 = w3
      } else if (idx === 1) {
        s1 = w2
        s2 = w3
        s3 = src[idx - 1]

        t3 = charType(s3)
      } else if (idx === 2) {
        s1 = w3
        s2 = src[idx - 2]
        s3 = src[idx - 1]

        t2 = charType(s2)
        t3 = charType(s3)
      } else {
        s1 = src[idx - 3]
        s2 = src[idx - 2]
        s3 = src[idx - 1]

        t1 = charType(s1)
        t2 = charType(s2)
        t3 = charType(s3)
      }

      s4 = src[idx]
      t4 = charType(s4)

      if (idx + 3 === len) {
        s5 = src[idx + 1]
        s6 = src[idx + 2]

        t5 = charType(s5)
        t6 = charType(s6)
      } else if (idx + 2 === len) {
        s5 = src[idx + 1]
        s6 = w4

        t5 = charType(s5)
        t6 = CharType.Others
      } else {
        s5 = w4
        s6 = w5

        t5 = CharType.Others
        t6 = CharType.Others
      }

      var score =
        start +
        bias("UP1", p1) +
        bias("UP2", p2) +
        bias("UP3", p3) +
        bias("BP1", p1 + p2) +
        bias("BP2", p2 + p3) +
        bias("UW1", s1) +
        bias("UW2", s2) +
        bias("UW3", s3) +
        bias("UW4", s4) +
        bias("UW5", s5) +
        bias("UW6", s6) +
        bias("BW1", s2 + s3) +
        bias("BW2", s3 + s4) +
        bias("BW3", s4 + s5) +
        bias("TW1", s1 + s2 + s3) +
        bias("TW2", s2 + s3 + s4) +
        bias("TW3", s3 + s4 + s5) +
        bias("TW4", s4 + s5 + s6) +
        bias("UC1", t1) +
        bias("UC2", t2) +
        bias("UC3", t3) +
        bias("UC4", t4) +
        bias("UC5", t5) +
        bias("UC6", t6) +
        bias("BC1", t2 + t3) +
        bias("BC2", t3 + t4) +
        bias("BC3", t4 + t5) +
        bias("TC1", t1 + t2 + t3) +
        bias("TC2", t2 + t3 + t4) +
        bias("TC3", t3 + t4 + t5) +
        bias("TC4", t4 + t5 + t6) +
        bias("UQ1", p1 + t1) +
        bias("UQ2", p2 + t2) +
        bias("UQ3", p3 + t3) +
        bias("BQ1", p2 + t2 + t3) +
        bias("BQ2", p2 + t3 + t4) +
        bias("BQ3", p3 + t2 + t3) +
        bias("BQ4", p3 + t3 + t4) +
        bias("TQ1", p2 + t1 + t2 + t3) +
        bias("TQ2", p2 + t2 + t3 + t4) +
        bias("TQ3", p3 + t1 + t2 + t3) +
        bias("TQ4", p3 + t2 + t3 + t4)

      var p = CharType.Others
      if (score > 0) {
        result.push(word)
        word = ""
        p = CharType.Internal
      }

      p1 = p2
      p2 = p3
      p3 = p

      word += s4
    }

    result.push(word)
    return result.filter(x => x.length !== 0)
  }

  return tokenizer
}

export const tokenize: (src: string) => Array<string> = createJapaneseTokenizer(
  -332,
  require("./dict.json")
)
