const assert = require('assert');
const lib    = require('../src/lib.ts');

describe('japanese-tokenizer', () => {
  describe('#charType', () => {
    it('should returns valid chartypes:', () => {
      const { charType, CharType } = lib;

      [
        ["一", CharType.KanjiNum],
        ["二", CharType.KanjiNum],
        ["三", CharType.KanjiNum],
        ["四", CharType.KanjiNum],
        ["五", CharType.KanjiNum],
        ["六", CharType.KanjiNum],
        ["七", CharType.KanjiNum],
        ["八", CharType.KanjiNum],
        ["九", CharType.KanjiNum],
        ["十", CharType.KanjiNum],
        ["百", CharType.KanjiNum],
        ["千", CharType.KanjiNum],
        ["万", CharType.KanjiNum],
        ["億", CharType.KanjiNum],
        ["兆", CharType.KanjiNum],

        ["龠", CharType.Kanji],
        ["々", CharType.Kanji],
        ["〆", CharType.Kanji],
        ["ヵ", CharType.Kanji],
        ["ヶ", CharType.Kanji],

        ["ぁ", CharType.Hiragana],
        ["ん", CharType.Hiragana],

        ["ァ", CharType.Katakana],
        ["ヴ", CharType.Katakana],
        ["ー", CharType.Katakana],
        ["ｱ", CharType.Katakana],
        ["ﾝ", CharType.Katakana],
        ["ﾞ", CharType.Katakana],
        ["ｰ", CharType.Katakana],

        ["a", CharType.Alphabet],
        ["z", CharType.Alphabet],
        ["A", CharType.Alphabet],
        ["Z", CharType.Alphabet],
        ["ａ", CharType.Alphabet],
        ["ｚ", CharType.Alphabet],
        ["Ａ", CharType.Alphabet],
        ["Ｚ", CharType.Alphabet],

        ["0", CharType.Numbers],
        ["9", CharType.Numbers],
        ["０", CharType.Numbers],
        ["９", CharType.Numbers],

        ["ω", CharType.Others],
      ].forEach(([ char, result ]: [string, string]) => {
        assert( charType(char) === result );
      });
    });
  });

  describe('#tokenize', () => {
    it('should returns tokenized Array<string>', () => {
      const { tokenize } = lib;

      assert.deepEqual(
        tokenize("今日は良い天気ですね"),
        ["今日", "は", "良い", "天気", "です", "ね"]
      );

      assert.deepEqual(
        tokenize("私の名前は中野です"),
        ["私","の","名前","は","中野","です"],
      );

      assert.deepEqual(
        tokenize("TinySegmenterは25kBで書かれています。"),
        ["TinySegmenter", "は", "2", "5", "kB", "で", "書か", "れ", "て", "い", "ます", "。"]
      );

      assert.deepEqual(
        tokenize("うらにわにはにわにわとりがいる"),
        ["うら","にわ","に","は","にわ","にわ","とり","が","いる"]
      );
    });
  })
});
